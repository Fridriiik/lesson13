#pragma once
#include <math.h.>

int SquaredAmount(int a, int b) {
	int sum = a + b;
 	return pow(double(sum), 2);
}