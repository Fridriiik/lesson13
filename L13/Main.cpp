#include <iostream>
#include <conio.h>
#include "Helpers.h"

int main() {
	int a, b; 
	std::cout << "Returns the square of the sum of two numbers\n";

	while (!_kbhit()) {
		std::cout << "Enter the first number - ";
		std::cin >> a;
		std::cout << "Enter the second number - ";
		std::cin >> b;
		std::cout << "Squared amount = " << SquaredAmount(a, b) << "\n";
		std::cout << "Press any key to continue or press enter to exit\n";
		if (_getch() == 13) return 0;
	}
}
